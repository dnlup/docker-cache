import fastify from "fastify";

const app = fastify({
  logger: true
})

app.get('/', async () => {
  return { ok: true }
})

app.listen({ port: 3000 })
