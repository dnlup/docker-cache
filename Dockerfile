FROM node:18-alpine3.17

COPY package.json package-lock.json .
RUN npm ci --omit=dev

COPY src src

EXPOSE 3000

CMD ["node", "src/index.mjs"]
